import 'package:epitech_flutter_quiz/queue.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/background-home.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: Container(
            padding: const EdgeInsets.all(12),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(child: FlutterLogo(size: 300)),
                SizedBox(height: 12),
                Row(
                  children: <Widget>[
                    SizedBox(
                      width: 100,
                      height: 100,
                      child: CircleAvatar(
                          backgroundColor: Colors.amberAccent,
                          backgroundImage:
                              NetworkImage('https://i.pravatar.cc/100')),
                    ),
                    SizedBox(width: 12),
                    Flexible(
                      child: TextField(
                          decoration: const InputDecoration(
                        helperText: '(Try to be cute)',
                        helperStyle:
                            TextStyle(fontSize: 12.0, color: Colors.white),
                        hintText: 'Enter your Pseudo...',
                        hintStyle:
                            TextStyle(fontSize: 20.0, color: Colors.white),
                      )),
                    ),
                  ],
                ),
                SizedBox(height: 12),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      height: 50,
                      child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) {
                              return QueuePage();
                            },
                          ));
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text('JOIN', style: TextStyle(fontSize: 20)),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
